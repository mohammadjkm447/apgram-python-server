import json


class Response:
    def __init__(self, RESPONSE_TYPE, clientSocket, responseUUID, isSuccess, failureType=0):
        self.RESPONSE_TYPE = RESPONSE_TYPE
        self.clientSocket = clientSocket
        self.responseUUID = responseUUID
        self.isSuccess = isSuccess
        self.failedType = failureType

    def setData(self, **kwargs):
        raise NotImplemented

    def createWrappedJsonData(self):
        data = self.createDictData()
        return json.dumps({
            'response_type': self.RESPONSE_TYPE,
            'uuid': self.responseUUID,
            'is_success': self.isSuccess,
            'failure_type': self.failedType,
            'data': data,
        })

    def createDictData(self):
        raise NotImplemented

class LoginResponse(Response):
    RESPONSE_TYPE = 1

    def __init__(self, clientSocket, responseUUID, isSuccess, failureType=0):
        super().__init__(LoginResponse.RESPONSE_TYPE, clientSocket, responseUUID, isSuccess, failureType)
        self.userUUID = None

    def setData(self, userUUID):
        self.userUUID = userUUID

    def createDictData(self):
        return {'user_uuid': self.userUUID}

class CreateAccountResponse(Response):
    RESPONSE_TYPE = 2

    def __init__(self, clientSocket, responseUUID, isSuccess, failureType=0):
        super().__init__(CreateAccountResponse.RESPONSE_TYPE, clientSocket, responseUUID, isSuccess, failureType)
        self.userUUID = None

    def setData(self, userUUID):
        self.userUUID = userUUID

    def createDictData(self):
        return {'user_uuid': self.userUUID}