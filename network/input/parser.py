import json
from .requests import *

REQUEST_TYPE_MAPPER = {
    1: LoginRequest,
    2: CreateAccountRequest,
}

def parseRequest(strData, clientSocket, isLoggedIn=True):
    jsonData = json.loads(strData)
    type = jsonData['request_type']
    uuid = jsonData['uuid']
    data = jsonData['data']
    if not isLoggedIn:
        if type != 2 or type != 1:
            return None
        return REQUEST_TYPE_MAPPER[type](clientSocket, uuid, data)
    if type in REQUEST_TYPE_MAPPER:
        return REQUEST_TYPE_MAPPER[type](clientSocket, uuid, data)
    raise ValueError('value of request type is not correct')
