import threading

from djangoApp.database.models import *
from network.manager import SocketManager
from network.output.responses import *

class Request:
    def __init__(self, REQUEST_TYPE, clientSocket, requestUUID, jsonData):
        self.REQUEST_TYPE = REQUEST_TYPE
        self.clientSocket = clientSocket
        self.requestUUID = requestUUID
        self.jsonData = jsonData

    def parseData(self):    # parse json data to variables
        raise NotImplemented

    def processRequest(self) -> Response:   # process variables
        raise NotImplemented

    def do(self):
        self.parseData()
        response = self.processRequest()
        if response is not None:
            self.clientSocket.sendResponse(response)

class LoginRequest(Request):
    REQUEST_TYPE = 1

    def __init__(self, clientSocket, requestUUID, jsonData):
        super().__init__(self.REQUEST_TYPE, clientSocket, requestUUID, jsonData)
        self.username = None
        self.password = None

    def parseData(self):
        self.username = self.jsonData['username']
        self.password = self.jsonData['password']

    def processRequest(self):
        passHash = self.password    # todo use hashed password
        user = User.objects.filter(username=self.username, passwordHash=passHash)
        if user.exists():   # it has successfully logged in
            user = user.first()
            self.clientSocket.setUser(user)
            SocketManager.getInstance().setOnline(self.clientSocket)
            threading.Thread(target=self.clientSocket.checkChanges).start()
            response = LoginResponse(self.clientSocket, self.requestUUID, isSuccess=True)
            response.setData(userUUID=user.uuid)
        else:   # no user match the username and password exist
            response = LoginResponse(self.clientSocket, self.requestUUID, isSuccess=False)
        return response

class CreateAccountRequest(Request):
    REQUEST_TYPE = 2

    def __init__(self, clientSocket, requestUUID, jsonData):
        super().__init__(CreateAccountRequest.REQUEST_TYPE, clientSocket, requestUUID, jsonData)
        self.username = None
        self.password = None
        self.firstname = None

    def parseData(self):
        self.username = self.jsonData['username']
        self.password = self.jsonData['password']
        self.firstname = self.jsonData['firstname']

    def processRequest(self):
        user = User.objects.filter(username=self.username)
        if user.exists():   # user with username already exists
            response = CreateAccountResponse(self.clientSocket, self.requestUUID, isSuccess=False)
        else:
            passHash = self.password    # todo hash the password
            user = User(
                username=self.username,
                passwordHash=passHash,
            )
            user.save()
            self.clientSocket.setUser(user)
            SocketManager.getInstance().setOnline(self.clientSocket)
            response = CreateAccountResponse(self.clientSocket, self.requestUUID, isSuccess=True)
            response.setData(user.uuid)
        return response
