import logging
import threading

from .input.parser import parseRequest
from .manager import SocketManager

CHARSET = 'utf-8'
FIRST_TIME_DATA_CHECK = "APGRAM V2"

class ClientSocket:
    def __init__(self, socket, address):
        self.socket = socket
        self.address = address
        self._login = False
        self._user = None

    @property
    def userUUID(self):
        return self._user.uuid

    def setUser(self, user):
        self._user = user

    def isLogin(self):
        return self._login

    def sendResponse(self, response):
        self.send(response.createWrappedJsonData())

    def send(self, data: str):
        count = len(str)
        self.socket.sendall(count.to_bytes(2, "big"))
        self.socket.sendall(data.encode(CHARSET))

    def get(self):
        length = int.from_bytes(self.socket.recv(4), 'big')
        return self.socket.recv(length).decode(CHARSET)

    def checkChanges(self):
        # todo : check changes that user should know
        pass

    def receiver(self): # receiver thread that use get method to get datas from socket
        try:
            first = self.get()
            if first.decode(CHARSET) != FIRST_TIME_DATA_CHECK:
                logging.warning(f"Client first time value check failed: {self.address}")
                self.socket.close()
                return
            while not self._login:  # if not logged in, only request with type 1 or 2 is allowed
                data = self.get()
                request = parseRequest(data, self, isLoggedIn=False)
                request.do()
            while True:
                data = self.get()
                request = parseRequest(data, self)
                threading.Thread(target=request.do).start()
        except: # when client disconnect
            if self._login:
                SocketManager.getInstance().setOffline(self)
