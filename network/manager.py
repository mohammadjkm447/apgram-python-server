
class SocketManager(object):
    singletonObject = None

    def __init__(self):
        SocketManager.singletonObject = self
        self.onlineClients = {}

    @staticmethod
    def getInstance():
        if SocketManager.singletonObject is None:
            SocketManager()
        return SocketManager.singletonObject

    def setOnline(self, clientSocket):
        self.onlineClients[clientSocket.userUUID] = clientSocket

    def setOffline(self, clientSocket):
        self.onlineClients.pop(clientSocket.userUUID)
