import os
import socket
import threading
import logging
from network.socket import ClientSocket

PORT = 12345
BIND_ADDRESS = '0.0.0.0'

def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'djangoApp.settings')
    import django
    django.setup()
    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serverSocket.bind((BIND_ADDRESS, PORT))
    logging.warning(f'Listening on ({BIND_ADDRESS}, {PORT})')
    serverSocket.listen()
    while True:
        s, addr = serverSocket.accept()
        logging.warning(f"Client Accepted: {addr}")
        client = ClientSocket(s, addr)
        threading.Thread(target=client.receiver).start()

if __name__ == '__main__':
    main()
