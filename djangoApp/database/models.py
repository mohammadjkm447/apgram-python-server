import datetime

from django.db import models
from .tools import uuidCreator

class User(models.Model):
    uuid = models.CharField(max_length=32, primary_key=True, default=uuidCreator)
    username = models.CharField(max_length=32, blank=False)
    passwordHash = models.CharField(max_length=32)     # todo : find a type for password hash
    createTime = models.DateTimeField(default=datetime.datetime.now)
    hasIcon = models.BooleanField(default=False)

    # todo set unique for username

class Chat(models.Model):
    uuid = models.CharField(max_length=32, primary_key=True, default=uuidCreator)
    type = models.CharField(max_length=5, choices=(('USER', 'USER'), (' GROUP', 'GROUP'), ('CHANNEL', 'CHANNEL')))
    creator = models.ForeignKey(User, on_delete=models.PROTECT)
    chatName = models.CharField(max_length=32, blank=True)  # if the type is GROUP or CHANNEL
    hasIcon = models.BooleanField(default=False) # used if type is not USER


class ChatMember(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    chat = models.ForeignKey(Chat, on_delete=models.PROTECT)
    role = models.CharField(max_length=10, choices=(('OWNER', 'OWNER'), ('ADMIN', 'ADMIN'), ('MEMBER', 'MEMBER')))


class Message(models.Model):
    uuid = models.CharField(max_length=32)
    chat = models.ForeignKey(Chat, on_delete=models.PROTECT)
    text = models.TextField()
    type = models.CharField(max_length=8, choices=(("TEXT", "TEXT"), ("VOICE", "VOICE"), ("IMAGE", "IMAGE"), ("VIDEO", "VIDEO"), ("FILE", "FILE")))
    fileName = models.CharField(max_length=32, blank=True)
    sender = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    sendDateTime = models.DateTimeField(default=datetime.datetime.now)


class Changes(models.Model):
    pass
    # todo : find needed column for changes that will check for every user
