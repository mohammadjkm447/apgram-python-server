from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


INSTALLED_APPS = [
    'djangoApp.database',
    # 'django.contrib.admin',
    # 'django.contrib.contenttypes',
]

# ROOT_URLCONF = 'djangoApp.urls'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
